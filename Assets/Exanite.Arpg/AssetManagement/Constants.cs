﻿namespace Exanite.Arpg.AssetManagement
{
    public static class Constants
    {
        public static readonly string PackageFileExtension = "packageinfo";
        public static readonly string AssetBundleFileExtension = "assetbundle";
    }
}
